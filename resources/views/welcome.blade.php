@extends('layout.master')
@section('Home')
@section('content')
    @if (request()->get('welcome'))
        <script>
            if (!alert("Email Varified Successfully {{ request()->get('welcome') }}")) {
                window.location.href = "/";
            }
        </script>
    @endif
    <div id="root"></div>
@endsection
